v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 120 -250 120 -230 {
lab=VSS}
N 440 -230 440 -210 {
lab=den}
N 120 -170 120 -150 {
lab=VSS}
N 60 -200 80 -200 {
lab=VSS}
N 120 -200 150 -200 {
lab=VSS}
N 380 -260 400 -260 {
lab=in}
N 440 -310 440 -290 {
lab=out}
N 440 -260 470 -260 {
lab=VSS}
N 440 -370 440 -350 {
lab=out}
N 380 -400 400 -400 {
lab=in}
N 440 -450 440 -430 {
lab=denb}
N 440 -400 510 -400 {
lab=VDD}
N 470 -260 510 -260 {
lab=VSS}
N 150 -200 190 -200 {
lab=VSS}
N 440 -500 440 -480 {
lab=denb}
N 380 -530 400 -530 {
lab=ENB}
N 440 -580 440 -560 {
lab=VDD}
N 440 -530 510 -530 {
lab=VDD}
N 440 -100 440 -80 {
lab=VSS}
N 380 -130 400 -130 {
lab=EN}
N 440 -180 440 -160 {
lab=den}
N 440 -130 470 -130 {
lab=VSS}
N 470 -130 510 -130 {
lab=VSS}
N 440 -210 440 -180 {
lab=den}
N 440 -480 440 -450 {
lab=denb}
C {devices/iopin.sym} 40 -510 0 0 {name=p1 lab=VDD}
C {devices/iopin.sym} 40 -490 0 0 {name=p2 lab=VSS}
C {devices/iopin.sym} 30 -460 0 0 {name=p3 lab=in}
C {devices/iopin.sym} 70 -460 0 0 {name=p4 lab=out}
C {devices/lab_pin.sym} 120 -150 0 0 {name=l1 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 60 -200 0 0 {name=l2 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 120 -250 0 0 {name=l3 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 190 -200 2 0 {name=l4 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 440 -190 2 0 {name=l5 sig_type=std_logic lab=den
}
C {devices/lab_pin.sym} 510 -260 2 0 {name=l6 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 380 -260 0 0 {name=l7 sig_type=std_logic lab=in
}
C {devices/lab_pin.sym} 440 -310 2 0 {name=l8 sig_type=std_logic lab=out
}
C {devices/lab_pin.sym} 440 -350 2 0 {name=l9 sig_type=std_logic lab=out
}
C {devices/lab_pin.sym} 380 -400 0 0 {name=l10 sig_type=std_logic lab=in
}
C {devices/lab_pin.sym} 440 -470 2 0 {name=l11 sig_type=std_logic lab=denb
}
C {devices/lab_pin.sym} 510 -400 2 0 {name=l12 sig_type=std_logic lab=VDD
}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 420 -260 0 0 {name=XN
w=4
l=18n
nf=2
model=nmos4_standard
spiceprefix=X
}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 100 -200 0 0 {name=XDUM
w=4
l=18n
nf=2
model=nmos4_standard
spiceprefix=X
}
C {BAG_prim/pmos4_standard/pmos4_standard.sym} 420 -400 0 0 {name=XP
w=4
l=18n
nf=2
model=pmos4_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 380 -530 0 0 {name=l14 sig_type=std_logic lab=ENB
}
C {devices/lab_pin.sym} 440 -580 2 0 {name=l15 sig_type=std_logic lab=VDD
}
C {devices/lab_pin.sym} 510 -530 2 0 {name=l16 sig_type=std_logic lab=VDD
}
C {BAG_prim/pmos4_standard/pmos4_standard.sym} 420 -530 0 0 {name=ENB
w=4
l=18n
nf=2
model=pmos4_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 440 -80 2 0 {name=l17 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 510 -130 2 0 {name=l18 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 380 -130 0 0 {name=l19 sig_type=std_logic lab=EN
}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 420 -130 0 0 {name=EN
w=4
l=18n
nf=2
model=nmos4_standard
spiceprefix=X
}
C {devices/iopin.sym} 40 -430 0 0 {name=p5 lab=EN}
C {devices/iopin.sym} 40 -410 0 0 {name=p6 lab=ENB}
