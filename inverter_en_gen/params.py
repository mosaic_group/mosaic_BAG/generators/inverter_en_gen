#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *


@dataclass
class inverter_en_layout_params(LayoutParamsBase):
    """
    Parameter class for inverter_en_gen

    Args:
    ----
    ntap_w : Union[float, int]
        Width of the N substrate contact

    ptap_w : Union[float, int]
        Width of P substrate contact

    lch : float
        Channel length of the transistors

    th_dict : Dict[str, str]
        NMOS/PMOS threshold flavor dictionary

    ndum : int
        Number of fingers in NMOS transistor

    tr_spaces : Dict[str, int]
        Track spacing dictionary

    tr_widths : Dict[str, Dict[int, int]]
        Track width dictionary

    top_layer: int
        Top metal Layer used in Layout

    ndum_mid_stages : int
        number of dummy fingers/transistors between stages/columns of transistors

    w_dict : Dict[str, Union[float, int]]
        NMOS/PMOS width dictionary

    seg_dict : Dict[str, int]
        NMOS/PMOS number of segments dictionary

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels

    out_conn : str
        Enable Alternative structure for flipped well processes
    """

    ntap_w: Union[float, int]
    ptap_w: Union[float, int]
    lch: float
    th_dict: Dict[str, str]
    ndum: int
    tr_spaces: Dict[Union[str, Tuple[str, str]], Dict[int, Union[float, int]]]
    tr_widths: Dict[str, Dict[int, int]]
    top_layer: int
    ndum_mid_stages: int
    w_dict: Dict[str, Union[float, int]]
    seg_dict: Dict[str, int]
    guard_ring_nf: int
    show_pins: bool
    out_conn: str

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> inverter_en_layout_params:
        return inverter_en_layout_params(
            ntap_w=17,
            ptap_w=17,
            lch=min_lch,
            th_dict={
                'Dummy_NB': 'standard', 'Dummy_NT': 'standard',
                'Dummy_PB': 'standard', 'Dummy_PT': 'standard',
                'n': 'standard', 'p': 'standard'
            },
            ndum=4,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            top_layer=5,
            ndum_mid_stages=2,
            w_dict={
                'Dummy_NB': 17, 'Dummy_NT': 17,
                'Dummy_PB': 17, 'Dummy_PT': 17,
                'n': 100, 'p': 100
            },
            seg_dict={'n': 20, 'p': 40},
            guard_ring_nf=0,
            show_pins=True,
            out_conn='g',
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> inverter_en_layout_params:
        return inverter_en_layout_params(
            ntap_w=17 * min_lch,
            ptap_w=17 * min_lch,
            lch=min_lch,
            th_dict={
               'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
               'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
               'n': 'lvt', 'p': 'lvt'
            },
            ndum=4,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            top_layer=5,
            ndum_mid_stages=2,
            w_dict={
               'Dummy_NB': 17 * min_lch, 'Dummy_NT': 17 * min_lch,
               'Dummy_PB': 17 * min_lch, 'Dummy_PT': 17 * min_lch,
               'n': 20 * min_lch, 'p': 20 * min_lch
            },
            seg_dict={'n': 20, 'p': 40},
            guard_ring_nf=2,
            show_pins=True,
            out_conn='g',
        )


@dataclass
class inverter_en_params(GeneratorParamsBase):
    layout_parameters: inverter_en_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> inverter_en_params:
        return inverter_en_params(
            layout_parameters=inverter_en_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
