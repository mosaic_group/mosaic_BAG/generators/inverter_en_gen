from typing import *
from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID

from sal.row import *
from sal.transistor import *

from .params import inverter_en_layout_params


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='inverter_en_layout_params parameter object',
        )

    def draw_layout(self):
        params: inverter_en_layout_params = self.params['params']

        horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['sig1', 'IN'],
                           wire_names_ds=['I']
                           )

        row_nmos = Row(name='n',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['sig1', 'IN'],
                       wire_names_ds=['I', 'O']
                       )

        row_Dummy_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['sig1', 'IN'],
                           wire_names_ds=['I', 'O']
                           )

        row_Dummy_PB = Row(name='Dummy_PB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PB'],
                           threshold=params.th_dict['Dummy_PB'],
                           wire_names_g=['sig1', 'IN'],
                           wire_names_ds=['OUT']
                           )

        row_pmos = Row(name='p',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['sig1', 'IN'],
                       wire_names_ds=['OUT', 'sig1']
                       )

        row_pmos_en = Row(name='p_en',
                          orientation=RowOrientation.R0,
                          channel_type=ChannelType.P,
                          width=params.w_dict['p'],
                          threshold=params.th_dict['p'],
                          wire_names_g=['sig1', 'IN'],
                          wire_names_ds=['OUT', 'sig1']
                          )

        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['sig1', 'IN'],
                           wire_names_ds=['OUT', 'sig1']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_NB, row_nmos, row_Dummy_NT, row_Dummy_PB, row_pmos, row_pmos_en, row_Dummy_PT])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n'] // divide
        fg_p = params.seg_dict['p'] // divide

        nmos = Transistor(name='n', row=row_nmos, fg=fg_n, deff_net='out', seff_net='den')
        nmos_en = Transistor(name='EN', row=row_nmos, fg=fg_n, deff_net='den')
        pmos = Transistor(name='p', row=row_pmos, fg=fg_p, deff_net='out', seff_net='denb')
        pmos_en = Transistor(name='ENB', row=row_pmos_en, fg=fg_p, deff_net='denb')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [nmos, nmos_en, pmos, pmos_en]

        # 3:   Calculate transistor locations

        fg_single = max((2 * nmos.fg) + 8, pmos.fg)  # NOTE: with +6 we had a short in LVS
                                                     #       we should add some calculation here
                                                     #       to get the generator more robust against param changes
        fg_total = fg_single + 2 * params.ndum
        fg_dum = params.ndum

        # Calculate positions of transistors
        nmos_en.assign_column(offset=fg_dum, fg_col=fg_single//2, align=TransistorAlignment.LEFT)
        nmos.assign_column(offset=fg_dum + fg_single//2, fg_col=fg_single//2, align=TransistorAlignment.RIGHT)
        pmos.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)
        pmos_en.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        nmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        nmos_en.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.DOWN)
        pmos.set_matched_direction(reference=nmos, seff_dir=TransistorDirection.UP, aligned=True)
        pmos_en.set_matched_direction(reference=pmos, seff_dir=TransistorDirection.UP, aligned=False)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=0,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Define horizontal tracks on which connections will be made
        row_nmos_idx = rows.index_of_same_channel_type(row_nmos)
        row_pmos_idx = rows.index_of_same_channel_type(row_pmos)
        row_pmos_en_idx = rows.index_of_same_channel_type(row_pmos_en)

        tid_nmos_G = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='IN')
        tid_nmos1_G = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='sig1')
        tid_pmos_G = self.get_wire_id('pch', row_pmos_idx, 'g', wire_name='IN')
        tid_pmos_D = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='OUT')
        tid_pmos1_G = self.get_wire_id('pch', row_pmos_en_idx, 'g', wire_name='IN')
        tid_pmos1_D = self.get_wire_id('pch', row_pmos_en_idx, 'ds', wire_name='OUT')
        tid_nmos_D = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='O')

        # 7:  Perform wiring
        # getting pitch of each metal layer
        pitch_hm = self.grid.get_track_pitch(horiz_conn_layer, unit_mode=True)
        pitch_vm = self.grid.get_track_pitch(vert_conn_layer, unit_mode=True)
        pitch_tm = self.grid.get_track_pitch(vert_conn_layer + 1, unit_mode=True)

        warr_n_G = self.connect_to_tracks([nmos.g], tid_nmos_G)
        warr_n1_G = self.connect_to_tracks([nmos_en.g], tid_nmos1_G)
        warr_p_G = self.connect_to_tracks([pmos.g], tid_pmos_G)
        warr_p1_G = self.connect_to_tracks([pmos_en.g], tid_pmos1_G)

        # Calculation of number of tracks required base on metal layer pitch and distance
        num2 = (pmos.d.upper_unit - pmos.d.lower_unit) // (1 * pitch_hm)
        if num2 < 2:
            num2 = 2

        num3 = (pmos.g.lower_unit - nmos.d.upper_unit) // (1 * pitch_hm)
        if num3 == 0:
            num3 = 1
        
        # Horizontal track_ids on metal 4
        tid_D_hor1 = TrackID(
            layer_id=4,
            track_idx=tid_pmos_D.base_index,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )

        tid_S1_hor1 = TrackID(
            layer_id=4,
            track_idx=tid_pmos_D.base_index + 1,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )

        tid_D_horp1 = TrackID(
            layer_id=4,
            track_idx=tid_pmos1_D.base_index,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )
        tid_S1_horp1 = TrackID(
            layer_id=4,
            track_idx=tid_pmos1_D.base_index + 1,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )

        tid_S1_hor2 = TrackID(
            layer_id=4,
            track_idx=tid_pmos1_D.base_index + num2,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=1,
        )

        tid_D_hor2 = TrackID(
            layer_id=4,
            track_idx=tid_nmos_D.base_index,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )
        tid_D_hor3 = TrackID(
            layer_id=4,
            track_idx=tid_nmos_D.base_index + num2,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num3,
            pitch=1,
        )
        tid_S_hor2 = TrackID(
            layer_id=4,
            track_idx=tid_nmos_D.base_index + 1,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=2,
        )
        tid_S_hor3 = TrackID(
            layer_id=4,
            track_idx=tid_nmos_G.base_index - 1 - num2//2,
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
            num=num2//2,
            pitch=1,
        )

        # connection of vertical tracks using M4 track_ids
        warr_p_D = self.connect_to_tracks([pmos.d, nmos.d], tid_D_hor1) 
        warr_n_D = self.connect_to_tracks([nmos.d], tid_D_hor2)
        warr_n1_S1 = self.connect_to_tracks([nmos_en.s], tid_D_hor2)
        warr_p1_D = self.connect_to_tracks([ pmos.s, pmos_en.d], tid_D_horp1)
        warr_n_S = self.connect_to_tracks([nmos.s, nmos_en.d], tid_S_hor2)

        num = (warr_p_D.upper_unit - warr_p_D.lower_unit) // (1 * pitch_vm)
        if num == 0:
            num = 1

        # Vertical track_ids on metel 5
        tid_G_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord= warr_p_D.lower_unit,
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig1'),
        )

        # Gate connection of XP and XN with 'in' net
        self.connect_to_tracks(
            [warr_n_G, warr_p_G],
            tid_G_vert,
            min_len_mode=0,
        )

        # 8:  Connections to substrate, and dummy fill
        # Connections to VSS
        self.connect_to_substrate('ptap', [nmos_en.s])
        # Connections to VDD
        self.connect_to_substrate('ntap', [pmos_en.s])

        # Add Pin
        self.add_pin('in', [warr_p_G, warr_n_G], show=params.show_pins)
        self.add_pin('EN', [warr_n1_G], show=params.show_pins)
        self.add_pin('ENB', [warr_p1_G], show=params.show_pins)
        self.add_pin('out', [warr_p_D, warr_n_D], show=params.show_pins)

        # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        # export supplies
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class inverter_en(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
